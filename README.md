[![pipeline status](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-single-cell/badges/main/pipeline.svg)](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-single-cell/-/commits/main)
[![Latest Release](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-single-cell/-/badges/release.svg)](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-single-cell/-/releases)


# singularity-single-cell

This is the DCGC singularity image for single cell analysis.

## How to build

```bash
# Build image
# GITHUB_PAT: GitHub personal access token for downloads, see https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens
# COMPILE_CPUS: Number of threads used for source compiling
# ANNOTATION_HUB_CACHE: AnnotationHub variable
singularity build --env COMPILE_CPUS=4 \
                  --env GITHUB_PAT=XYZ \
                  --env ANNOTATION_HUB_CACHE=/tmp/AnnotationHub \
                  --writable-tmpfs -f -F singularity-single-cell.sif \
                  Singularity
```

## How to run

The image contains a conda environment `single-cell` with R and python installations as well as packages for the analysis of single-cell and spatial data. In addition, there is a RStudio server and a Jupyter Lab.

### Juypter Lab

Start jupyter lab on port 8888:

```bash
singularity run --writable-tmpfs --app jupyter singularity-single-cell.sif --port 8888
```

Follow the link provided in the command line to start Jupyter lab. Then select a kernel: 

- Python: Single-cell analysis in python with scanpy and other tools
- R: Single-cell analysis in R with Seurat and other tools


### Pythons using command line

You can use the image to run python code on the command line:

```bash
# Run script
singularity run --writable-tmpfs --app python singularity-single-cell.sif my_script.py

# Run python interpreter
singularity run --writable-tmpfs --app python singularity-single-cell.sif
```

### RStudio server

Start rstudio server listening on port 8787:

```bash
mkdir -p rstudio-server/run rstudio-server/lib
singularity run -B $(pwd)/rstudio-server/run:/var/run/rstudio-server -B $(pwd)/rstudio-server/lib:/var/lib/rstudio-server --writable-tmpfs \
--env RSTUDIO_USER=$(whoami) --env RSTUDIO_PASSWORD=notsafe --env RSTUDIO_PORT=8787 --app rserver singularity-single-cell.sif
```

Browse to `localhost:8787` and login with your user name and the password `notsafe`.

Default is R. If you want to run python code in RStudio, go to `Global Options` in RStudio and set the python interpreter to `/opt/conda/envs/single-cell/bin/python3`.

### R and Rscript using command line

You can run R and Rscript on the command line:

```bash
# Interactive R
singularity run --writable-tmpfs --app R singularity-single-cell.sif

# Rscript
singularity run --writable-tmpfs --app Rscript singularity-single-cell.sif my_script.R
```

## R - python interoperability

### rpy2

If you want to run chunks of R within python you can do this via (rpy2)[https://rpy2.github.io/doc/v3.0.x/html/index.html]:

```python
import numpy as np
import pandas as pd
import rpy2.robjects.packages as packages
import rpy2.robjects.lib.ggplot2 as ggplot2
import rpy2.robjects as ro
R = ro.r
datasets = packages.importr('datasets')
mtcars = packages.data(datasets).fetch('mtcars')['mtcars']
gp = ggplot2.ggplot(mtcars)
pp = (gp 
      + ggplot2.aes_string(x='wt', y='mpg')
      + ggplot2.geom_point(ggplot2.aes_string(colour='qsec'))
      + ggplot2.scale_colour_gradient(low="yellow", high="red") 
      + ggplot2.geom_smooth(method='auto') 
      + ggplot2.labs(title="mtcars", x='wt', y='mpg'))

pp.plot()
```

In a Jupyter notebook you can also run R code directly via the rpy2 magic:

```python
%load_ext rpy2.ipython
```

```R
%%R

library(ggplot2)
ggplot(mtcars, aes(x=hp, y=mpg, color=cyl)) +
    geom_point(size=3)
```

Please have a look at the rpy2 documentation for more configuration (e.g. how to pass python variables to R and back).


### reticulate

If you want to run chunks of python within a RStudio/R document you can do this via (reticulate)[https://rstudio.github.io/reticulate/index.html]:

```
library(reticulate)

# For single-cell analysis in python
use_condaenv("python")

sc = import("scanpy")
```

Please have a look at the reticulate documentation for more configuration (e.g. how to pass R variables to python and back).

## List of packages

To get a list of python packages and libraries/tools installed in the `single-cell` environment do:

```bash
# in image
eval "$(micromamba shell hook --shell bash)"
micromamba activate single-cell
conda list
```

To get a list of R packages installed in the `single-cell` environment do:

```bash
# in image
eval "$(micromamba shell hook --shell bash)"
micromamba activate single-cell
R --no-init-file --slave -e 'installed.packages()'
```
