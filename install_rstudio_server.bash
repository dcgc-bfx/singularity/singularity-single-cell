  #!/bin/bash
 
 set -euo pipefail
 
 ################################################################################################################################
 # Installs RStudio server                                                                                                      #
 #                                                                                                                              #
 # Copied and adapted from from https://github.com/rocker-org/rocker-versioned2/blob/master/scripts/install_rstudio.sh (Rocker) #
 ################################################################################################################################
  
# RStudio version
RSTUDIO_VERSION=$1

# Install OS packages for RStudio server
apt-get update --fix-missing -q
apt-get install -y -q\
	ca-certificates \
	lsb-release \
  	file \
  	git \
  	libapparmor1 \
  	libclang-dev \
  	libcurl4-openssl-dev \
  	libedit2 \
  	libobjc4 \
  	libssl-dev \
  	libpq5 \
  	psmisc \
  	procps \
  	pwgen \
  	sudo \
  	wget

# Download RStudio Server for Ubuntu 18-21 (use UBUNTU_CODENAME 'jammy' for Ubuntu >=22)
ARCH=$(dpkg --print-architecture)
DOWNLOAD_FILE=rstudio-server.deb
UBUNTU_CODENAME="jammy"
wget "https://download2.rstudio.org/server/${UBUNTU_CODENAME}/${ARCH}/rstudio-server-${RSTUDIO_VERSION/"+"/"-"}-${ARCH}.deb" -O "$DOWNLOAD_FILE" 

  
# Install RStudio 
dpkg -i "$DOWNLOAD_FILE"
rm "$DOWNLOAD_FILE"
chmod -R a+rw /var/lib/rstudio-server

# Issue https://github.com/rocker-org/rocker-versioned2/issues/137
rm -f /var/lib/rstudio-server/secure-cookie-key


# RStudio wants an /etc/R, will populate from $R_HOME/etc
mkdir -p /etc/R


# Use more robust file locking to avoid errors when using shared volumes:
echo "lock-type=advisory" >/etc/rstudio/file-locks


# Log to stderr
cat <<EOF >/etc/rstudio/logging.conf
[*]
log-level=warn
logger-type=syslog
EOF


# Set up rserver.conf
cat <<"EOF" >/etc/rstudio/rserver.conf
###############################
# R Server Configuration File #
###############################

# Minimum user id 
auth-minimum-user-id=100

# If set to 1 no authentication neccessary. If set to 0 then authentication via script.
auth-none=0

# Script for authentication in /usr/lib/rstudio-server/bin
auth-pam-helper-path=pam-helper

# The number of minutes a user will stay logged in while idle before required to sign in again. If set to 0 then disabled.
auth-timeout-minutes=0

# The number of days to keep a user signed in when using the “Stay Signed In” option. If set to 0 then disabled.
auth-stay-signed-in-days=30
EOF

# Set up rsession.conf
cat <<"EOF" >/etc/rstudio/rsession.conf
################################
# R Session Configuration File #
################################

# Enable GitHub Copilot
copilot-enabled=1

# R session times out after this number of minutes
session-timeout-minutes=720

# When session times out, exit (0) or suspend (1) it
session-timeout-suspend=1
EOF

# Set up database.conf
cat <<"EOF" >/etc/rstudio/database.conf
provider=sqlite
directory=/var/lib/rstudio-server
EOF

# Script for basic authentication (PAM): set up USER and PASSWORD as environment variables
cat <<"EOF" >/usr/lib/rstudio-server/bin/pam-helper
#!/bin/bash
set -o nounset

## Enforces the custom password specified in the RSTUDIO_PASSWORD environment variable
## The accepted RStudio username is the same as the RSTUDIO_USER environment variable (i.e., local user name).

IFS='' read -r password

[ "${RSTUDIO_USER}" = "${1}" ] && [ "${RSTUDIO_PASSWORD}" = "${password}" ]
EOF
chmod +x /usr/lib/rstudio-server/bin/pam-helper

# Clean up
rm -rf /var/lib/apt/lists/*

# Check the RStudio Server
echo -e "Check the RStudio Server version...\n"

rstudio-server version

echo -e "\nInstall RStudio Server, done!"


