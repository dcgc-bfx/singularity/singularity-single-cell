Bootstrap: docker
From: mambaorg/micromamba:1.5.8-bookworm

%labels
    Author rost@mpi-cbg.de
    Organisation MPI-CBG
    Author andreas.petzold@tu-dresden.de
    Organisation DcGC Dresden

#########
# Help  #
#########

%help
  Container for single cell analysis.

  see https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-single-cell

#######################
# Files for building  #
#######################

%files
  tests /opt
  environment_single_cell.yml /opt
  packages_R.yml /opt
  install_rstudio_server.bash /rstudio-server-conda/install_rstudio_server.bash
  start_rstudio_server.bash /rstudio-server-conda/start_rstudio_server.bash
  
##########################
# Environment variables  #
##########################

%environment
  export PATH=${PATH}:/usr/local/bin/aws
  export NUMBA_CACHE_DIR=/tmp
  export MAMBA_ROOT_PREFIX=/opt/conda

#################
# Installation  #
#################

%post
  set -eu
  ls -l /opt/tests/data
 
  export DEBIAN_FRONTEND=noninteractive 
  export PATH=/opt/bin:${PATH}
  export LD_LIBRARY_PATH=/opt/lib:${LD_LIBRARY_PATH}

  # Build date (when building, pass via SINGULARITYENV_CONTAINER_BUILD_DATE otherwise date when building)
  if [ -z ${CONTAINER_BUILD_DATE+x} ]
  then
    CONTAINER_BUILD_DATE=$(date)
  fi
  echo "export CONTAINER_BUILD_DATE=\"${CONTAINER_BUILD_DATE}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository names (when building, pass via SINGULARITYENV_CONTAINER_GIT_NAME otherwise empty)
  if [ -z ${CONTAINER_GIT_NAME+x} ]
  then
    CONTAINER_GIT_NAME=''
  fi
  echo "export CONTAINER_GIT_NAME=\"${CONTAINER_GIT_NAME}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository url (when building, pass via SINGULARITYENV_CONTAINER_GIT_URL otherwise empty)
  if [ -z ${CONTAINER_GIT_URL+x} ]
  then
    CONTAINER_GIT_URL=''
  fi
  echo "export CONTAINER_GIT_URL=\"${CONTAINER_GIT_URL}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository commit id (when building, pass via SINGULARITYENV_CONTAINER_GIT_COMMIT_ID otherwise empty)
  if [ -z ${CONTAINER_GIT_COMMIT_ID+x} ]
  then
    CONTAINER_GIT_COMMIT_ID=''
  fi
  echo "export CONTAINER_GIT_COMMIT_ID=\"${CONTAINER_GIT_COMMIT_ID}\"" >> $SINGULARITY_ENVIRONMENT

  # Container version (when building, pass via SINGULARITYENV_CONTAINER_VERSION otherwise empty)
  if [ -z ${CONTAINER_VERSION+x} ]
  then
    CONTAINER_VERSION=''
  fi
  echo "export CONTAINER_VERSION=\"${CONTAINER_VERSION}\"" >> $SINGULARITY_ENVIRONMENT

  # the linux command 'make' supports the compilation of independent targets in parallel; this is also passed to R 'install.packages' since it uses 'make' internally
  if [ -z ${CONTAINER_COMPILE_CPUS+x} ]
  then
    COMPILE_CPUS=6
  else
    COMPILE_CPUS="${CONTAINER_COMPILE_CPUS}"
  fi
  export COMPILE_CPUS

  ##########
  # System #
  ##########
  
  # deactive interactive dialogs
  export DEBIAN_FRONTEND=noninteractive
  export TZ="Europe/Berlin"

  
  # install and reconfigure locale
  apt-get clean -q && apt-get update -y -q && apt-get upgrade -y -q
  apt-get install -y -q locales
  apt-get clean -q
  export LANGUAGE="en_US.UTF-8"
  export LANG="en_US.UTF-8"
  export LC_ALL="en_US.UTF-8"
  echo "LC_ALL=en_US.UTF-8" >> /etc/environment
  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
  echo "LANG=en_US.UTF-8" > /etc/locale.conf
  locale-gen --purge en_US.UTF-8
  dpkg-reconfigure --frontend=noninteractive locales
  

  # set PATH for use of conda/mamba; also add to system-wide /etc/profile
  #export PATH=/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
  #echo >> /etc/profile
  #echo 'export PATH=/opt/conda/bin:$PATH' >> /etc/profile
  #echo 'export LD_LIBRARY_PATH=/opt/conda/lib:/opt/conda/lib/server:$LD_LIBRARY_PATH' >> /etc/profile


  # System packages
  apt-get clean
  apt-get update --yes --fix-missing --quiet
  apt-get install --yes --quiet build-essential pkg-config apt-utils
  apt-get upgrade --yes  --quiet
  apt-get install -y -q\
    build-essential \
    gdebi-core \
    git \
    wget \
    unzip \
    libx11-dev libxt-dev \
    gsfonts-x11 \
    xfonts-base \
    xfonts-100dpi \
    xfonts-75dpi \
    xfonts-scalable


  # Aws cli tools (for cellxgene-census)
  # Install AWS CLI v2 following the official AWS documentation
  wget https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -O awscliv2.zip
  unzip awscliv2.zip
  ./aws/install

  # Clean up
  rm -rf awscliv2.zip ./aws

  #############################
  # RStudio Server and quarto #
  #############################

  # Quarto
  QUARTO_VERSION="1.5.57"
  wget --no-verbose https://github.com/quarto-dev/quarto-cli/releases/download/v${QUARTO_VERSION}/quarto-${QUARTO_VERSION}-linux-amd64.deb
  gdebi -q -n quarto-${QUARTO_VERSION}-linux-amd64.deb
  rm quarto-${QUARTO_VERSION}-linux-amd64.deb


  # Rstudio server
  bash /rstudio-server-conda/install_rstudio_server.bash "2024.09.0-375"


  # Clean apt files
  apt-get clean --quiet
  apt-get autoremove
  
  ################################
  # Python and conda environment #
  ################################
  
  # Install conda in base environment
  micromamba install --yes -n base -c conda-forge conda
  micromamba config -n base set pip_interop_enabled True
 
 
  # Install conda environment
  micromamba create --yes --name single-cell --file /opt/environment_single_cell.yml
  
  # install pytables without numexpr
  micromamba install --yes -c conda-forge -c bioconda -n single-cell pytables --no-deps
  
  # install tiledbsoma-py (for cellxgene-census; see below)
  micromamba install -c conda-forge -c tiledb -n single-cell "tiledbsoma-py!=1.14.1"
  
  # Set up some environment variables
  /opt/conda/bin/conda env config vars set LD_LIBRARY_PATH=/opt/conda/envs/single-cell/lib:${LD_LIBRARY_PATH} --name single-cell
  /opt/conda/bin/conda env config vars set TZDIR=/opt/conda/envs/single-cell/share/zoneinfo --name single-cell
  /opt/conda/bin/conda env config vars set TZ="Europe/Berlin" --name single-cell

  
  # Install non-conda python packages via pip, allow to install dependencies
  micromamba run --name single-cell pip --no-cache-dir install --upgrade-strategy only-if-needed \
    tangram-sc \
    comseg \
    diffxpy \
    singler \
    wot \
    pypath-omnipath \
    tcrdist3 \
    pephubclient \
    liana \
    blitzgsea \
    git+https://github.com/scverse/pertpy.git \
    git+https://github.com/saezlab/decoupler-py.git

  # Update packages where their current versions have bugs
  micromamba run --name single-cell pip --no-cache-dir install \
    spatial-image \
    spatialdata \
    spatialdata-io \
    spatialdata-plot \
    --upgrade
  
  # Install non-conda python packages via pip, do not allow to install dependencies
  SKLEARN_ALLOW_DEPRECATED_SKLEARN_PACKAGE_INSTALL=True micromamba run --name single-cell pip --no-cache-dir install --no-deps \
    git+https://github.com/BiomedicalMachineLearning/stLearn.git \
    palantir \
    cell2location \
    cellxgene-census \
    sopa
    
  
  # Clean up micromamba and pip
  micromamba run --name single-cell pip cache purge
  micromamba clean --all
  

  ####################
  # R and R packages #
  ####################

  # Remove existing R installation
  micromamba remove --name single-cell r-base
  
  # but reinstall anndata2ri, which was removed with the previous command
  micromamba install -c conda-forge -c bioconda -n single-cell anndata2ri --no-deps

  # Install R
  export R_VERSION=4.4.0
  
cat << EOF > install_R.sh

# Get R source code
wget -O R-${R_VERSION}.tar.gz -nv https://cran.rstudio.com/src/base/R-4/R-${R_VERSION}.tar.gz
tar -zxf R-${R_VERSION}.tar.gz
cd R-${R_VERSION}

# Install
PKG_CONFIG_PATH=/opt/conda/envs/lib/pkgconfig \
CC=x86_64-conda-linux-gnu-cc \
OBJC=x86_64-conda-linux-gnu-cc \
CFLAGS="-march=nocona -mtune=haswell -ftree-vectorize -fPIC -fstack-protector-strong -fno-plt -O2 -ffunction-sections -isystem /opt/conda/envs/single-cell/include" \
LDFLAGS="-Wl,-O2 -Wl,--sort-common -Wl,--as-needed -Wl,-z,relro -Wl,-z,now -Wl,--disable-new-dtags -Wl,--gc-sections -Wl,--allow-shlib-undefined -L/opt/conda/envs/single-cell/lib" \
CPPFLAGS="-DNDEBUG -D_FORTIFY_SOURCE=2 -O2 -isystem /opt/conda/envs/single-cell/include" \
CXXFLAGS="-fvisibility-inlines-hidden -fmessage-length=0 -march=nocona -mtune=haswell -ftree-vectorize -fPIC -fstack-protector-strong -fno-plt -O2 -ffunction-sections -pipe -isystem /opt/conda/envs/single-cell/include" \
./configure --prefix=/opt/conda/envs/single-cell \
--enable-R-profiling --enable-memory-profiling --enable-shared --enable-R-shlib \
--with-blas="-L/opt/conda/envs/single-cell/lib -Wl,--no-as-needed -lmkl_gf_lp64 -Wl,--start-group -lmkl_gnu_thread  -lmkl_core  -Wl,--end-group -fopenmp -ldl -lpthread -lm" --with-lapack \
--with-readline --with-pcre2 --with-tcltk --with-cairo --with-libpng --with-jpeglib --with-libtiff \
--with-recommended-packages --with-x --with-libdeflate-compression

make -j${COMPILE_CPUS}
make install
cd ..
rm -r R-${R_VERSION}.tar.gz R-${R_VERSION}

# Add some environment variables that should be included in R
echo >> /opt/conda/envs/single-cell/lib/R/etc/Renviron
echo "# These environment variables should be present in R, rstudio and rserver so add them to Renviron." >> /opt/conda/envs/single-cell/lib/R/etc/Renviron
echo "CONTAINER_BUILD_DATE=\"${CONTAINER_BUILD_DATE}\"" >> /opt/conda/envs/single-cell/lib/R/etc/Renviron
echo "CONTAINER_GIT_NAME=\"${CONTAINER_GIT_NAME}\"" >> /opt/conda/envs/single-cell/lib/R/etc/Renviron
echo "CONTAINER_GIT_COMMIT_ID=\"${CONTAINER_GIT_COMMIT_ID}\"" >> /opt/conda/envs/single-cell/lib/R/etc/Renviron
echo "CONTAINER_GIT_URL=\"${CONTAINER_GIT_URL}\"" >> /opt/conda/envs/single-cell/lib/R/etc/Renviron
echo "CONTAINER_VERSION=\"${CONTAINER_VERSION}\"" >> /opt/conda/envs/single-cell/lib/R/etc/Renviron
echo "MAMBA_ROOT_PREFIX=/opt/conda" >> /opt/conda/envs/single-cell/lib/R/etc/Renviron
echo "RETICULATE_PYTHON=/opt/conda/envs/single-cell/bin/python3" >> /opt/conda/envs/single-cell/lib/R/etc/Renviron

# Add some R code that should be set up at the start
echo >> /opt/conda/envs/single-cell/lib/R/etc/Rprofile.site
echo "options(reticulate.conda_binary='/usr/bin/micromamba', repos=c(CRAN='https://cloud.r-project.org'))" >> /opt/conda/envs/single-cell/lib/R/etc/Rprofile.site
EOF

  micromamba run --name single-cell bash ./install_R.sh
  rm ./install_R.sh
  #/opt/conda/bin/conda env config vars set R_HOME=/opt/conda/envs/single-cell/lib/R --name single-cell

  
  # Install R packages yaml, pak, BiocManager, maketools and withr for automated installation
  micromamba run --name single-cell R --no-init-file --slave -e 'options(Ncpus='"$COMPILE_CPUS"'); install.packages(c("yaml", "pak", "BiocManager", "maketools", "withr"))'
  
  # Install other R packages
cat << EOF > install_packages.R
# Number of cpus to compile
compile_cpus = Sys.getenv("COMPILE_CPUS")
if (nchar(compile_cpus) == 0) compile_cpus = 2
options(Ncpus=compile_cpus)

# For pak installer, do not install system dependencies
options(pkg.sysreqs=FALSE)

# Download static V8 libraries for V8 package
Sys.setenv(DOWNLOAD_STATIC_LIBV8 = 1)

# Download static arrow libraries for arrow package
Sys.setenv(LIBARROW_BINARY = TRUE)
Sys.setenv(LIBARROW_MINIMAL = FALSE)

# For a few packages, this part '-L/opt/conda/envs/single-cell/lib' in LDFLAGS causes problems
# Create a modified LDFLAGS for these packages
custom_ldflags = gsub('-L/opt/conda/envs/single-cell/lib', '', maketools::r_cmd_config("LDFLAGS"))

# Install these packages first
withr::with_makevars(c(LDFLAGS=custom_ldflags), install.packages("arrow"))
withr::with_makevars(c(LDFLAGS=custom_ldflags), BiocManager::install("lpsymphony"))

# Read packages from yaml and install the other packages
packages = yaml::read_yaml("/opt/packages_R.yml")
pak::pkg_install(packages, upgrade=TRUE)

# Install these packages separately since they have some conflicts 
pak::pkg_install(c("Azimuth=github::satijalab/azimuth","SeuratData=github::satijalab/seurat-data", "liana=github::saezlab/liana"), upgrade=TRUE)

pak::cache_clean()
EOF
  
  micromamba run --name single-cell R --no-init-file --slave --file=install_packages.R
  rm install_packages.R
  
  #########
  # Other #
  #########
  
  # Reinstall rpy2 so that it is configured with the correct R version
  micromamba run --name single-cell pip uninstall --yes rpy2
  micromamba run --name single-cell pip --no-cache-dir install --no-binary rpy2 rpy2
  
  
  # Install R kernel for juypter
  micromamba run --name single-cell R --no-init-file --slave -e 'IRkernel::installspec(user=FALSE, sys_prefix="/opt/conda/envs/single-cell")'


  # Permissions
  chmod -R a+w /opt
  chmod -R a+rw /rstudio-server-conda
  chmod +x /rstudio-server-conda/install_rstudio_server.bash
  chmod +x /rstudio-server-conda/start_rstudio_server.bash

#########
# Tests #
#########

%test
  # scanpy
  bash <<-EOF
    # Make sure conda/mamba root prefix is correct
    export MAMBA_ROOT_PREFIX=/opt/conda
    export CONDA_ENVS_DIRS=/opt/conda/envs

    cd /opt/tests
    micromamba config set use_lockfiles False && micromamba run --name single-cell python3 python_tests.py
EOF
  if [ $? -eq 0 ]; then
      echo "Python tests passed."
  else
      echo "Python test error!"
      exit 1
  fi


  # R
  bash <<-EOF
    # Make sure conda/mamba root prefix is correct
    export MAMBA_ROOT_PREFIX=/opt/conda
    export CONDA_ENVS_DIRS=/opt/conda/envs

    cd /opt/tests
    micromamba config set use_lockfiles False && micromamba run --name single-cell Rscript R_tests.R
EOF
  if [ $? -eq 0 ]; then
      echo "R tests passed."
  else
      echo "R test error!"
      exit 1
  fi

  
  
#################################
# rstudio server/R/Rscript apps #
#################################

%apprun rserver
  bash <<-EOF
    # Make sure conda/mamba root prefix is correct
    export MAMBA_ROOT_PREFIX=/opt/conda

    # Due to a mamba bug, micromamba run seems to ignore the root prefix if there is a user .condarc file with conda_envs settings.
    # This environment variable will fix it for now.
    export CONDA_ENVS_DIRS=/opt/conda/envs

    # Start rserver
    # Use the following environment variables: RSTUDIO_PORT, RSTUDIO_USER, RSTUDIO_PASSWORD.
    # If not set, sets RSTUDIO_PORT to 8787, RSTUDIO_USER to current user and RSTUDIO_PASSWORD to "notsafe".
    micromamba config set use_lockfiles False && micromamba run --name single-cell /rstudio-server-conda/start_rstudio_server.bash
EOF

%apprun R
  bash <<-EOF
    # Make sure conda/mamba root prefix is correct
    export MAMBA_ROOT_PREFIX=/opt/conda
    export CONDA_ENVS_DIRS=/opt/conda/envs
    
    # Start R
    micromamba config set use_lockfiles False && micromamba run  --name single-cell R "${@}"
EOF

%apprun Rscript
  bash <<-EOF
    # Make sure conda/mamba root prefix is correct
    export MAMBA_ROOT_PREFIX=/opt/conda
    export CONDA_ENVS_DIRS=/opt/conda/envs
    
    # Start Rscript
    micromamba config set use_lockfiles False && micromamba run --name single-cell Rscript "${@}"
EOF

###########################
# jupyter lab/python apps #
###########################

%apprun jupyter
  bash <<-EOF
    # Make sure conda/mamba root prefix is correct
    export MAMBA_ROOT_PREFIX=/opt/conda
    export CONDA_ENVS_DIRS=/opt/conda/envs
    
    # Start jupyter
    micromamba config set use_lockfiles False && micromamba run --name single-cell jupyter lab --no-browser "${@}"
EOF

%apprun python
  bash <<-EOF
    # Make sure conda/mamba root prefix is correct
    export MAMBA_ROOT_PREFIX=/opt/conda
    export CONDA_ENVS_DIRS=/opt/conda/envs
    
    # Start python3
    micromamba config set use_lockfiles False && micromamba run --name single-cell python3 "${@}"
EOF

