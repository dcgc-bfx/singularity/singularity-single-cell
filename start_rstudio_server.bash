#!/bin/bash

set -euo pipefail

# Set defaults for the environment variables PORT, USER and PASSWORD if not already set
RSTUDIO_PORT="${RSTUDIO_PORT:-8787}"
RSTUDIO_USER="${RSTUDIO_USER:-$(whoami)}"
RSTUDIO_PASSWORD="${RSTUDIO_PASSWORD:-notsafe}"
export RSTUDIO_PORT RSTUDIO_USER RSTUDIO_PASSWORD

# R Home
R_HOME="/opt/conda/envs/single-cell/lib/R"

# Source global definitions
source /etc/profile
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

# Source user-specific definitions
if [ -f $HOME/.bashrc ]; then
  source $HOME/.bashrc
fi

# Start rserver
echo /usr/lib/rstudio-server/bin/rserver \
	--www-address=127.0.0.1 \
	--rsession-which-r="${R_HOME}/bin/R" \
	--rsession-config-file=/etc/rstudio/rsession.conf \
	--auth-minimum-user-id=100 \
	--auth-none=0 \
	--auth-pam-helper-path=pam-helper \
	--auth-timeout-minutes=0 \
	--auth-stay-signed-in-days=30 \
	--www-port=$RSTUDIO_PORT \
	--server-user=$RSTUDIO_USER

/usr/lib/rstudio-server/bin/rserver \
	--www-address=127.0.0.1 \
	--rsession-which-r="${R_HOME}/bin/R" \
	--rsession-config-file=/etc/rstudio/rsession.conf \
	--auth-minimum-user-id=100 \
	--auth-none=0 \
	--auth-pam-helper-path=pam-helper \
	--auth-timeout-minutes=0 \
	--auth-stay-signed-in-days=30 \
	--www-port=$RSTUDIO_PORT \
	--server-user=$RSTUDIO_USER
